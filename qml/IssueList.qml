import QtQuick 2.7
import Lomiri.Components 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0

Page {
    anchors.fill: parent
    visible: false

    function updateModel(_model) {
        // Initialize the full list of
        myModel.sourceModel = _model
    }

    header: PageHeader {
        id: header
        title: i18n.tr('UReport')

        leadingActionBar {
            numberOfSlots: 0
            actions: [
                Action {
                    iconName: (isClosedVisible)?"select": "select-none"
                    text: i18n.tr('Display closed reports')
                    onTriggered: isClosedVisible = !isClosedVisible
                },
                Action {text: 'Version 0.4'} ]
        }
    }

    signal deleteIssue(var index)

    Component {
        id: myReport
        NewIssueForm {
            id: myIssueForm
            height: bottomEdge.height
            width: bottomEdge.width
        } // NewIssueForm
    } // Component

    Rectangle {
        id: boss
        anchors {
            top: header.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        Component {
        id: myDelegate

        ListItem {
            id: currentItem
            height: units.gu(6)

            leadingActions: ListItemActions {
                actions: [
                Action {
                    iconName: "delete"
                    onTriggered: {
                        if (debug) {console.log("IssueList: currentId="+model.id+" currentIndex="+index)}
                        // Signal for db deletion
                        deleteIssue(model.id)
                        // Remove item from the IssueList
                        myModel.remove(index)
                    }
                }
            ]}
            trailingActions: ListItemActions {
            actions: [
                Action {
                    iconName: "filters"
                    onTriggered: {
                        // Initialize issue object
                        editReport.setContent(model)
                        // Push the page on top of the stack
                        myPageStack.push(editReport) // TODO Do this in the IssueForm object
                    }
                }
            ]} // trailingActions

            Row {
            id: currentIssue
            anchors.centerIn: parent
            padding: units.gu(0.7)

            Rectangle {
                width: boss.width - units.gu(11)
                height: units.gu(5)
                Text { anchors.centerIn: parent; text: model.title; font.pixelSize: units.gu(2) ; width: parent.width }
            }

            Rectangle {
                width: units.gu(10)
                height: units.gu(5)
                Text { anchors.centerIn: parent; text: model.status; font.pixelSize: units.gu(2) ; width: parent.width }
            } // Rectangle : type specific field

            } // Row
        } // ListItem
        } // Component : delegate

        LomiriListView {
            id: issueList
            anchors.fill: parent
            currentIndex: -1
            model: myModel
            delegate: myDelegate
            pullToRefresh {
                id: refresher
                enabled: false
                onRefresh: myFct.refreshIssueList(model) // TODO Fetch each issue and update the list
            }
        } // LomiriListView

        // Custom Model allowing filtering out
        SortableProxyModel {
            id: myModel
            filterRole: "status"
            filterValue: (isClosedVisible)? "": "closed"
        }

        BottomEdge {
            id: bottomEdge
            height: parent.height + header.height
            hint.text: i18n.tr('Create New Issue')
            contentComponent: myReport

            onCommitStarted: {}
            onCommitCompleted: {header.visible = false}
            onCollapseStarted: {header.visible = true}
            onCollapseCompleted: {}
        } // BottomEdge
    } // Rectangle
}
