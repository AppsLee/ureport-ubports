import QtQuick 2.7
import Lomiri.Components 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0

Page {
    visible: false

    header: PageHeader {
        id: header
        title: i18n.tr('Create New Report')
    }

    property var _lineHeight: units.gu(6)
    property var _paddingValue: units.gu(1)
    property var _issueTitle
    property var _issueType
    property var _selectedRepo: mainRepoModel.get(0).project

    Rectangle {
        //color: LomiriColors.green
        anchors {
            top: header.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }


        // Type: issue, feature request, ...
        Rectangle {
            id: repoGrp
            height: _lineHeight
            width: parent.width
            //visible: false
            //anchors.top: titleGrp.bottom

            Rectangle {
                id: repoLbl
                height: _lineHeight
                width: units.gu(12)
                anchors.top: parent.top
                Label {
                    width: units.gu(12)
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    anchors.leftMargin: _paddingValue

                    text: i18n.tr('Project')
                    horizontalAlignment: Label.AlignLeft
                } // Label
            } // Rectangle
            OptionSelector {
                id: repoSel
                anchors.left: repoLbl.right
                anchors.right: parent.right
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.topMargin: units.gu(0.5)
                anchors.bottomMargin: units.gu(0.5)
                anchors.leftMargin: _paddingValue
                anchors.rightMargin: _paddingValue
                //text: i18n.tr('Variable repo: ')
                containerHeight: itemHeight * 2.7
                model: mainRepoModel//ListModel{id: repoList}
                delegate: Component {
                    OptionSelectorDelegate { text: name }
                }
                onDelegateClicked: {
                    _selectedRepo = mainRepoModel.get(index).project
                    if (debug) {console.log("NewIssueForm: repo selected "+mainRepoModel.get(index).project)}
                    repoGrp.height = _lineHeight
                }
                onCurrentlyExpandedChanged: {
                    if (currentlyExpanded) {
                        if (debug) {console.log("NewIssueForm: Expansion")}
                        repoGrp.height = containerHeight+units.gu(1)
                    }
                }
            } // OptionSelector
        } // Rectangle

        // Title
        Rectangle {
            id: titleGrp
            height: _lineHeight
            width: parent.width
            anchors.top: repoGrp.bottom
            Label {
                id: titleLbl
                width: units.gu(12)
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.leftMargin: _paddingValue

                text: i18n.tr('Title')
                horizontalAlignment: Label.AlignLeft
            } // Label
            TextField {
                id: titleField
                anchors.left: titleLbl.right
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter
                anchors.rightMargin: _paddingValue
            } // TextField: Input field for user variable
        } // Rectangle

        // Type: issue, feature request, ...
        Rectangle {
            id: typeGrp
            height: _lineHeight
            width: parent.width
            visible: false // TODO should I keep (or add) this feature
            anchors.top: titleGrp.bottom

            Rectangle {
                id: typeLbl
                height: _lineHeight
                width: units.gu(12)
                anchors.top: parent.top
                Label {
                    width: units.gu(12)
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    anchors.leftMargin: _paddingValue

                    text: i18n.tr('Type')
                    horizontalAlignment: Label.AlignLeft
                } // Label
            } // Rectangle
            OptionSelector {
                id: typeSel
                anchors.left: typeLbl.right
                anchors.right: parent.right
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.topMargin: units.gu(0.5)
                anchors.bottomMargin: units.gu(0.5)
                anchors.leftMargin: _paddingValue
                anchors.rightMargin: _paddingValue
                //text: i18n.tr('Variable type: ')
                containerHeight: itemHeight * 2
                model: [i18n.tr("Issue"),
                        i18n.tr("Incident")
                        ]
                onDelegateClicked: {
                    if (debug) {console.log("NewIssueForm: type selected "+index)}
                    _issueType = index
                    typeGrp.height = _lineHeight
                }
                onCurrentlyExpandedChanged: {
                    if (currentlyExpanded) {
                        if (debug) {console.log("NewIssueForm: Expansion")}
                        typeGrp.height = containerHeight+units.gu(1)
                    }
                }
            } // OptionSelector
        } // Rectangle

        // Description
        Rectangle {
            id: descriptionGrp
            height: _lineHeight*3
            width: parent.width
            anchors.top: typeGrp.bottom

            Label {
                id: descriptionLbl
                width: units.gu(12)
                //anchors.verticalCenter: parent.verticalCenter
                anchors.top: parent.top
                anchors.topMargin: _paddingValue
                anchors.left: parent.left
                anchors.leftMargin: _paddingValue

                text: i18n.tr('Description')
                horizontalAlignment: Label.AlignLeft
            } // Label
            TextArea {
                id: descriptionField
                anchors.top: parent.top
                anchors.topMargin: _paddingValue
                anchors.bottom: parent.bottom
                anchors.bottomMargin: _paddingValue
                anchors.left: descriptionLbl.right
                anchors.right: parent.right
                anchors.rightMargin: _paddingValue
            } // TextArea: Input field for user variable
        } // Rectangle

        // Attachment
        Rectangle { // TODO Add a way to import attachments like screenshots and logs
            id: attachmentGrp
            height: _lineHeight
            width: parent.width
            anchors.top: descriptionGrp.bottom
            //color: LomiriColors.lightRed
        } // Rectangle

        // Validation buttons
        Rectangle {
            id: validationGrp
            height: _lineHeight
            width: parent.width
            anchors.top: attachmentGrp.bottom

            Row {
            id: row
            anchors.centerIn: parent
            spacing: _paddingValue

                Button {
                    text: i18n.tr("OK")
                    color: LomiriColors.green
                    onClicked: {
                        myFct.createGitlabIssue(titleField.text, descriptionField.text, _selectedRepo);
                        bottomEdge.collapse()
                    }
                } // Button: OK

                Button {
                    text: i18n.tr("Cancel")
                    onClicked: {
                        if (debug) {console.log("NewIssueForm: Expansion"+_selectedRepo)}
                        bottomEdge.collapse()
                    }
                } // Button: Cancel
            } // Row
        } // Rectangle
    } // Rectangle
}
