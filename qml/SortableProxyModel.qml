import QtQuick 2.4
//import QtQuick.Layouts 1.1
import Lomiri.Components 1.3

ListModel {
    property var sourceModel
    property string filterRole: ""
    property string filterValue: ""

    onSourceModelChanged: {
        updateFilter();
    }

    onFilterRoleChanged: {
        updateFilter();
    }

    onFilterValueChanged: {
        updateFilter();
    }

    function updateFilter() {
        clear();
        if (!sourceModel || filterRole === "" || filterValue === "") {
            if (sourceModel) {
                for (var i = 0; i < sourceModel.count; i++) {
                    append(sourceModel.get(i));
                }
            }
        } else {
            // Filter the source model based on filterRole and filterValue
            for (var i = 0; i < sourceModel.count; i++) {
                var item = sourceModel.get(i);
                if (item[filterRole] !== filterValue) {
                    append(item);
                }
            }
        }
    }
}
