/*
 * Copyright (C) 2024  Richard Lee
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * ureport is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Lomiri.Components 1.3
import Qt.labs.settings 1.0
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'ureport.applee'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    Settings {
        id: settings
        property var showClosedIssues: false
    }
    ListModel {id: mainRepoModel}
    ListModel {id: issueModel}

    property bool debug: true
    property alias isClosedVisible: settings.showClosedIssues

    PageStack {
        id: myPageStack

        Component.onCompleted: {
            myPageStack.push(reportList)

            // Database connection
            myDb.initializeDatabase();
            // Initialize IssueList with known issues
            if (debug) {console.log("Main: call loadData()")}
            myDb.loadData(issueModel, mainRepoModel)
            reportList.updateModel(issueModel)
        }

        IssueList {
            id: reportList
            onDeleteIssue: function(data) {
                myFct.delIssue(data)
            }
        } // IssueList

        IssueForm {
            id: editReport
        } // IssueList
    } // PageStack

    Functions {
        id: myFct
        onIssueFetched: function(data) {
            // Pass the fetched data to populate the issue
            editReport.updateData(data)
        }
        onNotesFetched: function(data) {
            // Pass the fetched data to populate the issue
            editReport.updateNotes(data)
        }
        onRefreshList: reportList.updateModel(issueModel)
    }

    Database {
        id: myDb
    }
}
