import QtQuick 2.4
import QtQuick.Layouts 1.1
import Lomiri.Components 1.3
import Qt.labs.settings 1.0

Item { // Utility functions

    property string gitlabToken: "YOUR_API_KEY="
    property string gitlabApiUrl: "https://gitlab.com/api/v4/projects/"

    signal notesFetched(var data)
    signal issueFetched(var data)
    signal refreshList()

    // Fetch the list of notes from current issue
    function listIssueNotes(_project, _id) {
        var xhr = new XMLHttpRequest();
        xhr.open("GET", gitlabApiUrl+_project+"/issues/"+_id+"/notes", true);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.setRequestHeader("Authorization", "Bearer " + gitlabToken);

        xhr.onreadystatechange = function() {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                if (xhr.status === 200) {
                    if (debug) {console.log("IssueForm: Issue notes fetched successfully:", xhr.responseText)}
                    var ans = JSON.parse( xhr.responseText )

                    // Get the actual notes from the list
                    notesFetched(ans)
                } else {
                    console.log("IssueForm: Failed to get notes:", xhr.status, xhr.responseText);
                }
            }
        };

        xhr.send();
    }

    // Fetch information regarding the current issue
    function getGitlabIssue(_project, _id) {
        var xhr = new XMLHttpRequest();
        xhr.open("GET", gitlabApiUrl+_project+"/issues/"+_id, true);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.setRequestHeader("Authorization", "Bearer " + gitlabToken);

        xhr.onreadystatechange = function() {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                if (xhr.status === 200) {
                    if (debug) {console.log("IssueForm: Issue fetched successfully:", xhr.responseText)}
                    var ans = JSON.parse( xhr.responseText )

                    // Save data I use
                    issueFetched(ans)
                } else {
                    console.log("IssueForm: Failed to get issue:", xhr.status, xhr.responseText);
                }
            }
        };

        xhr.send();
    }

    // Create a new issue
    function createGitlabIssue(_title, _description, _project) {
        var xhr = new XMLHttpRequest();
        xhr.open("POST", gitlabApiUrl+_project+"/issues", true);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.setRequestHeader("Authorization", "Bearer " + Qt.atob(gitlabToken));

        xhr.onreadystatechange = function() {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                if (xhr.status === 201) {
                    if (debug) {console.log("NewIssueForm: Issue created successfully:", xhr.responseText)}
                    var ans = JSON.parse( xhr.responseText )

                    // Data I want to keep
                    updateIssue(ans, true)
                } else {
                    console.log("NewIssueForm: Failed to create issue:", xhr.status, xhr.responseText);
                }
            }
        };

        var data = JSON.stringify({
            title: _title,
            description: _description
        });

        xhr.send(data);
    }

    // Create a new issue
    function updateGitlabIssue(_iid, _title, _description, _project) {
        var xhr = new XMLHttpRequest();
        xhr.open("PUT", gitlabApiUrl+_project+"/issues/"+_iid, true);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.setRequestHeader("Authorization", "Bearer " + gitlabToken);

        xhr.onreadystatechange = function() {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                if (xhr.status === 200) {
                    if (debug) {console.log("NewIssueForm: Issue updated successfully:", xhr.responseText)}
                    var ans = JSON.parse( xhr.responseText )

                    // Data I want to keep
                    updateIssue(ans, false)
                } else {
                    console.log("NewIssueForm: Failed to update issue:", xhr.status, xhr.responseText);
                }
            }
        };

        var data = JSON.stringify({
            title: _title,
            description: _description
        });

        xhr.send(data);
    }

    // Delete ths issue from IssueList
    function delIssue(_idx) {
        if (debug) {console.log("Function: delIssue #"+_idx)}

        // Save this issue in myDb
        myDb.removeData(_idx)
    }

    // Add new issue to the IssueList
    function updateIssue(_data, _isNew) {
        if (debug) {console.log("Function: updateIssue begin")}
        var modelData = {
            id: _data.id,
            iid: _data.iid,
            project: _data.project_id,
            title: _data.title,
            status: _data.state,
        }
        // Append data to the IssueList
        if (_isNew) {
            issueModel.append(modelData)
            refreshList() // Signal Main to update the model for IssueList
        }
        // Save this issue in myDb
        myDb.saveData(modelData)
        if (debug) {console.log("Function: updateIssue ends")}
    }
}
