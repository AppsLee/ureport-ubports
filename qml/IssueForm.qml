import QtQuick 2.7
import Lomiri.Components 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0

Page {
    visible: true

    header: PageHeader {
        id: header
        title: ""
        trailingActionBar {
            numberOfSlots: 2
            actions: [
                Action {
                    iconName: (isReadOnly)?"edit": "edit-clear"
                    text: i18n.tr('Display closed reports')
                    onTriggered: isReadOnly = !isReadOnly
                }]
        }
    }

    property var _lineHeight: units.gu(6)
    property var _paddingValue: units.gu(1)
    property var _issueID
    property var _issueTitle
    property var _issueType
    property var localIssueModel
    property var isReadOnly: true
    //property ListModel localNotesModel

    // Triggered by signal received after getGitlabIssue()
    function updateNotes(_data) {
        if (debug) {console.log("IssueForm: function begin updateNotes()")}

        // Built a model list of notes
        for (var i = 0; i < _data.length; i++) {
            localNotesModel.append({
                noteId: _data[i].id,
                author: _data[i].author.name,
                body: _data[i].body
            });
        }

        if (debug) {console.log("IssueForm: function end updateNotes()")}
    }

    // Triggered by signal received after getGitlabIssue()
    function updateData(_data) {
        if (debug) {console.log("IssueForm: function begin updateData()")}

        // IssueList data model
        var modelData = {
            id: _data.id,
            iid: _data.iid,
            project: _data.project_id,
            title: _data.title,
            status: _data.state,
        }

        // Save this issue in myDb
        myDb.saveData(modelData)

        // Update the form
        titleField.text = _data.title
        descriptionField.text = _data.description
        descriptionField.text += "\n comments number: "+_data.user_notes_count
        upvoteLbl.text = _data.upvotes + " upvotes"
        downvoteLbl.text = _data.downvotes + " downvotes"

        // Update the list item
        localIssueModel.title = _data.title // Update the title
        localIssueModel.status = _data.state // Update the status

        if (debug) {console.log("IssueForm: function end updateData()")}
    }

    // Triggered when opening an issue
    function setContent(_model) {
        header.title = _model.title
        titleField.text = _model.title
        localIssueModel = _model // Required to update the status in the IssueList

        // Clear the form
        localNotesModel.clear()
        descriptionField.text = ""
        upvoteLbl.text = ""
        downvoteLbl.text = ""

        myFct.getGitlabIssue(_model.project, _model.iid)
        myFct.listIssueNotes(_model.project, _model.iid)
    }

    ListModel {
        id: localNotesModel
    }

    Rectangle {
        //color: LomiriColors.green
        anchors {
            top: header.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        // Title
        Rectangle {
            id: titleGrp
            visible: !isReadOnly
            height: _lineHeight
            width: parent.width
            Label {
                id: titleLbl
                width: units.gu(12)
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.leftMargin: _paddingValue

                text: i18n.tr('Title')
                horizontalAlignment: Label.AlignLeft
            } // Label
            TextField {
                id: titleField
                readOnly: isReadOnly
                anchors.left: titleLbl.right
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter
                //anchors.leftMargin: _paddingValue
                anchors.rightMargin: _paddingValue
            } // TextField: Input field for user variable
        } // Rectangle

        // Type: issue, feature request, ...
        Rectangle {
            id: typeGrp
            visible: false
            height: _lineHeight
            width: parent.width
            anchors.top: titleGrp.bottom

            Rectangle {
                id: typeLbl
                height: _lineHeight
                width: units.gu(12)
                anchors.top: parent.top
                Label {
                    width: units.gu(12)
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    anchors.leftMargin: _paddingValue

                    text: i18n.tr('Type')
                    horizontalAlignment: Label.AlignLeft
                } // Label
            } // Rectangle
            OptionSelector {
                id: typeSel
                anchors.left: typeLbl.right
                anchors.right: parent.right
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.topMargin: units.gu(0.5)
                anchors.bottomMargin: units.gu(0.5)
                anchors.leftMargin: _paddingValue
                anchors.rightMargin: _paddingValue
                //text: i18n.tr('Variable type: ')
                containerHeight: itemHeight * 2
                model: [i18n.tr("Issue"),
                        i18n.tr("Incident")
                        ]
                onDelegateClicked: {
                    if (debug) {console.log("IssueForm: type selected "+index)}
                    _issueType = index
                    typeGrp.height = _lineHeight
                }
                onCurrentlyExpandedChanged: {
                    if (currentlyExpanded) {
                        if (debug) {console.log("IssueForm: Expansion")}
                        typeGrp.height = containerHeight+units.gu(1)
                    }
                }
            } // OptionSelector
        } // Rectangle

        // Description
        Rectangle {
            id: descriptionGrp
            height: _lineHeight*3
            width: parent.width
            anchors.top: typeGrp.bottom

            Label {
                id: descriptionLbl
                width: units.gu(12)
                //anchors.verticalCenter: parent.verticalCenter
                anchors.top: parent.top
                anchors.topMargin: _paddingValue
                anchors.left: parent.left
                anchors.leftMargin: _paddingValue

                text: i18n.tr('Description')
                horizontalAlignment: Label.AlignLeft
            } // Label
            TextArea {
                id: descriptionField
                readOnly: isReadOnly
                anchors.top: parent.top
                anchors.topMargin: _paddingValue
                anchors.bottom: parent.bottom
                anchors.bottomMargin: _paddingValue
                anchors.left: descriptionLbl.right
                anchors.right: parent.right
                anchors.rightMargin: _paddingValue
            } // TextArea: Input field for user variable
        } // Rectangle

        // Votes
        Rectangle {
            id: votesGrp
            height: _lineHeight
            width: parent.width
            anchors.top: descriptionGrp.bottom

            Icon {
                id: upvoteIco
                width: units.gu(5)
                height: units.gu(5)
                anchors.top: parent.top
                anchors.topMargin: _paddingValue
                anchors.left: parent.left
                anchors.leftMargin: _paddingValue
                name:   "thumb-up"
                color:  LomiriColors.slate
            }

            Label {
                id: upvoteLbl
                width: units.gu(12)
                anchors.top: parent.top
                anchors.topMargin: _paddingValue
                anchors.left: upvoteIco.right
                anchors.leftMargin: _paddingValue

                text: "0 upvotes"
                horizontalAlignment: Label.AlignLeft
            } // Label

            Icon {
                id: downvoteIco
                width: units.gu(5)
                height: units.gu(5)
                anchors.top: parent.top
                anchors.topMargin: _paddingValue
                anchors.left: parent.left
                anchors.leftMargin: (_paddingValue * 2) + (parent.width / 2)
                name:   "thumb-down"
                color:  LomiriColors.slate
            }

            Label {
                id: downvoteLbl
                width: units.gu(12)

                anchors.top: parent.top
                anchors.topMargin: _paddingValue
                anchors.left: downvoteIco.right
                anchors.leftMargin: _paddingValue

                text: "0 downvotes"
                horizontalAlignment: Label.AlignLeft
            } // Label
        } // Rectangle

        // Attachment
        Rectangle {
            id: attachmentGrp
            height: _lineHeight
            width: parent.width
            anchors.top: votesGrp.bottom
            //color: LomiriColors.lightRed
        } // Rectangle

        // Notes list
        Rectangle {
            id: notesGrp
            height: _lineHeight*4 // TODO
            width: parent.width
            anchors.top: attachmentGrp.bottom

            LomiriListView {
                id: noteList
                anchors.fill: parent
                currentIndex: -1
                model: localNotesModel
                delegate: ListItem {
                    Rectangle {
                        width: parent.width
                        height: implicitHeight

                        Label {
                            id: descriptionLbl
                            anchors.right: parent.right
                            anchors.left: parent.left
                            anchors.leftMargin: _paddingValue

                            text: model.author
                        } // Label
                        TextArea {
                            id: descriptionField
                            readOnly: true
                            wrapMode: Text.Wrap

                            width: parent.width
                            //height: implicitHeight
                            implicitHeight: contentItem.height
                            anchors.top: descriptionLbl.bottom

                            text: model.body
                        } // TextArea: Input field for user variable
                    }
                }
            } // LomiriListView
        }
        // Validation
        Rectangle {
            id: validationGrp
            height: _lineHeight
            width: parent.width
            anchors.top: notesGrp.bottom

            Row {
            id: row
            anchors.centerIn: parent
            spacing: _paddingValue

                Button {
                    text: i18n.tr("Update")
                    color: LomiriColors.green
                    onClicked: {
                        myFct.updateGitlabIssue(localIssueModel.iid, titleField.text, descriptionField.text, localIssueModel.project);
                        localIssueModel.title = titleField.text
                        myPageStack.pop()
                    }
                } // Button: OK

                Button {
                    text: i18n.tr("Cancel")
                    onClicked: {
                        myPageStack.pop()
                    }
                } // Button: Cancel
            } // Row
        } // Rectangle
    } // Rectangle
}
