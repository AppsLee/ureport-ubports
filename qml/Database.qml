import QtQuick 2.4
import QtQuick.LocalStorage 2.0
import Lomiri.Components 1.3


Item { // Utility functions

    function getDatabase() {
        return LocalStorage.openDatabaseSync("UReportDB", "0.2", "Settings", 10000000);
    }

    function initializeDatabase() {
        if (debug) {console.log("Database: Init")}
        var db = LocalStorage.openDatabaseSync("UReportDB", "", "Settings", 10000000);

        // Create tables if no DB
        db.transaction(function(tx) {
            // Create the issues table if it doesn't exist
            tx.executeSql('CREATE TABLE IF NOT EXISTS Issues(id INTEGER NOT NULL PRIMARY KEY, sModel TEXT)');
            // Create the repository table if it doesn't exist
            tx.executeSql('CREATE TABLE IF NOT EXISTS Repositories(id INTEGER NOT NULL UNIQUE, url TEXT NOT NULL, name TEXT)');

            var res = tx.executeSql('SELECT count(*) as nb FROM Repositories');
            if (res.rows.item(0).nb < 3) {
                if (debug) {console.log("Database: Initialize the repository table")}
                // Populate the default Repositories
                tx.executeSql('INSERT OR IGNORE INTO Repositories VALUES(?, ?, ?)', [58070742, encodeURIComponent("AppsLee/ureport-ubports"), "UReport"]);
                tx.executeSql('INSERT OR IGNORE INTO Repositories VALUES(?, ?, ?)', [52994603, encodeURIComponent("ubports/ubuntu-touch"), "ubuntu-touch"]);
                tx.executeSql('INSERT OR IGNORE INTO Repositories VALUES(?, ?, ?)', [10237257, encodeURIComponent("ubports/development/apps/lomiri-camera-app"), "Camera App"]);
            }
        })

        // Handle previous versions of the database
        if (db.version == "0.1") {
            db.changeVersion("0.1", "0.2", function(tx) {
                if (debug) {console.log("Database: Update to v0.2")}
                tx.executeSql('ALTER TABLE Repositories ADD COLUMN name');
                tx.executeSql('UPDATE Repositories SET name="UReport" WHERE id = 58070742');
                tx.executeSql('UPDATE Repositories SET name="ubuntu-touch" WHERE id = 52994603');
                tx.executeSql('UPDATE Repositories SET name="Camera App" WHERE id = 10237257');
            })
        }

        if (debug) {console.log("Database: Init done")}
    }

    function saveData(sModel) {
        if (debug) {console.log("Database: Save")}
        var db = getDatabase();
        db.transaction(function(tx) {
            tx.executeSql('INSERT OR REPLACE INTO Issues VALUES(?,?)', [sModel.id,JSON.stringify(sModel)]);
        })
        if (debug) {console.log("Database: Saved")}
    }

    function removeData(id) {
        if (debug) {console.log("Database: Delete")}
        var db = getDatabase();
        db.transaction(function(tx) {
            tx.executeSql('DELETE FROM Issues WHERE id=?', [id]);
        })
        if (debug) {console.log("Database: Deleted")}
    }

    function loadData(_modelRef, _modelRepo) {

        if (debug) {console.log("Database: Load")}

        var db = getDatabase();
        var modelData

        db.transaction(function(tx) {
            var res = tx.executeSql('SELECT sModel FROM Issues');
            if (res.rows.length > 0) {
                if (debug) {console.log("Database: content")}
                for (var i=0; i<res.rows.length; i++)
                {
                    modelData = JSON.parse(res.rows.item(i).sModel);
                    if (debug) {console.log("Item #"+i+": "+res.rows.item(i).sModel)}
                    _modelRef.append(modelData)
                }
            } else {
                console.log("Database: Empty DB")
            }

            var res = tx.executeSql('SELECT * FROM Repositories');
            if (res.rows.length > 0) {
                if (debug) {console.log("Database: content")}
                for (var i=0; i<res.rows.length; i++)
                {
                    modelData = {
                        project: res.rows.item(i).id,
                        encodedUrl: res.rows.item(i).url,
                        url: decodeURIComponent(res.rows.item(i).url),
                        name: res.rows.item(i).name
                    }
                    if (debug) {console.log("Item #"+i+": "+JSON.stringify(modelData))}
                    _modelRepo.append(modelData)
                }
            } else {
                console.log("Database: Empty DB")
            }

        })

        if (debug) {console.log("Database: Loaded")}
    }
}
